# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

if {[lsearch [namespace children] ::tcltest] == -1} {
	package require tcltest 2.2.5
	namespace import ::tcltest::*
}

package require TLC

test form_default_handling-1.1 {Check correct order of precedence with no default values, no _onchange} -body { #<<<
	tlc::Form .f -name "form_default_handling-1.1" -schema {
		"Foo"		{foo radiogroup -choices {
			"option 1"	{option1}
			"option 2"	{option2}
			"option 3"	{option3}
		}}
		"Bar"		{bar checkbutton}
		"History"	{history entry}
		"Thing"		{thing entry}
	}
	.f set_data	[dict create \
			foo		"option2" \
			bar		0 \
			history	"Updated" \
			thing	"Also updated" \
	]
	set dat	[.f get_data]
	list [dict get $dat foo] [dict get $dat bar] [dict get $dat history] [dict get $dat thing]
} -cleanup {
	if {[itcl::is object .f]} {delete object .f}
} -result [list option2 0 Updated "Also updated"]
#>>>
test form_default_handling-1.2 {Check correct order of precedence with default values, no _onchange} -body { #<<<
	tlc::Form .f -name "form default_handling-1.2" -schema {
		"Foo"		{foo radiogroup -choices {
			"option 1"	{option1}
			"option 2"	{option2}
			"option 3"	{option3}
		}}
		"Bar"		{bar checkbutton}
		"History"	{history entry}
		"Thing"		{thing entry}

		_defaults {
			foo		"option1"
			bar		1
			history	"Foo"
		}
	}
	.f set_data	[dict create \
			foo		"option2" \
			bar		0 \
			history	"Updated" \
			thing	"Also updated" \
	]
	set dat	[.f get_data]
	list [dict get $dat foo] [dict get $dat bar] [dict get $dat history] [dict get $dat thing]
} -cleanup {
	if {[itcl::is object .f]} {delete object .f}
} -result [list option2 0 Updated "Also updated"]
#>>>

::tcltest::cleanupTests
return



