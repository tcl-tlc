DESTDIR=

MYDESTDIR=$(DESTDIR)/usr/lib/tlc
TEAPOT=/var/teapot

TCLSH = TCLLIBPATH="." tclsh8.5

all: scripts

scripts: scripts-stamp

scripts-stamp: scripts/*.itk scripts/*.itcl
	./make_tclIndex.tcl
	touch scripts-stamp

install: all
	install -d $(MYDESTDIR)
	install -d $(MYDESTDIR)/scripts
	install -d $(MYDESTDIR)/scripts/images
	install -d $(MYDESTDIR)/tools
	install --mode 644 pkgIndex.tcl $(MYDESTDIR)
	install --mode 644 init.tcl $(MYDESTDIR)
	install --mode 644 scripts/*.itk $(MYDESTDIR)/scripts
	install --mode 644 scripts/*.itcl $(MYDESTDIR)/scripts
	install --mode 644 scripts/tclIndex $(MYDESTDIR)/scripts
	-install --mode 644 scripts/images/*.gif $(MYDESTDIR)/scripts/images
	-install --mode 644 scripts/images/*.xbm $(MYDESTDIR)/scripts/images

teapot: all
	install -d teapot
	-rm -f teapot/*
	teapot-pkg generate --output teapot --type zip .

install-teapot: teapot
	teapot-admin add $(TEAPOT) teapot/*.zip

refresh-teapot: teapot
	teacup remove TLC
	teacup install teapot/*

force-refresh-teapot: teapot
	teacup remove TLC
	teacup install --force --timeout 1 teapot/*-TLC-*

uninstall:
	-rm -rf $(MYDESTDIR)

test: all
	$(TCLSH) tests/all.tcl $(TESTFLAGS)

clean:
	-rm -rf scripts/tclIndex scripts-stamp teapot
