# vim: foldmarker=<<<,>>>

package require TLC-base

namespace eval :: {
	package require Itk 3.1

	namespace import -force ::itcl::*
	namespace import -force ::itk::*

	package require blttable
	namespace eval ::blt {
		proc table {args} {
			return [uplevel [list ::blttable::table] $args]
		}
	}
}

namespace eval ::tlc {
	namespace eval ::blt {}
	namespace eval ::blttable {namespace export *}

	variable version	0.99.0

	variable guilibrary \
			[file normalize [file join [pwd] [file dirname [info script]]]]

	namespace import ::blt::*
	if {[info commands ::blttable::table] != ""} {
		catch {namespace import ::blttable::*}
	}
}

lappend auto_path [file join $::tlc::guilibrary scripts]

if {[info exists tlc::custom_theme_object]} {
	set tlc::theme		$tlc::custom_theme_object ::#auto]
} else {
	if {[info exists ::tlc_UserThemeOverride]} {
		set tlc::theme		[tlc::Daftheme ::#auto]
	} else {
		set tlc::theme		[tlc::Theme ::#auto]
	}
}

package provide TLC $::tlc::version

# Set saner defaults

bind Text <Key-Tab> {tk_focusNext %W}
bind Text <Shift-Key-Tab> {tk_focusPrev %W}

tlc::Baseconfig tlc::default_config -configfile ""
array set tlc::config	[tlc::default_config dump_config]

# Default options <<<

foreach {opt settingkey} {
	*activeBackground					background
	*highlightBackground				highlightbackground
	*highlightColor						highlightcolor
	*font								font
	*labelFont							font
	*textFont							font
	*selectBorderWidth					selectborderwidth
	*selectBackground					selectbackground
	*selectForeground					selectforeground
	*Tabset*selectBackground			background
	*textBackground						textbackground
	*Listbox.background					textbackground
	*disabledBackground					disabledbackground
	*Entry.disabledForeground			disabledforeground
	*Text.disabledForeground			disabledforeground
	*Text.TextHighlightBackground		texthighlightbackground
	*.hidebg							hidebg
	*enabledBackground					textbackground
	*TreeView.background				textbackground
	*TreeView.borderwidth				borderwidth
	*TreeView.highlightThickness		borderwidth
	*Labelframe.font					boldfont
	*labelFont							boldfont
	*Button.padY						button_pady
	*Form.padding						formpadding
} {
	option add $opt [$tlc::theme setting $settingkey] startupFile
}

# Hacks.  userdefault priority == 60
foreach {opt settingkey} {
	*Entry.background					textbackground
	*Entry.highlightBackground			background
	*TreeCtrl.background				textbackground
} {
	option add $opt [$tlc::theme setting $settingkey]	61
}

switch -exact $tcl_platform(platform) {
	"unix"		{
		option add *Scrollbar.width					12	startupFile
		option add *Scrollbar.borderWidth			1	startupFile
		option add *sbWidth							12	startupFile
		option add *scrollMargin					0	startupFile
		option add *elementBorderWidth				1	startupFile
		option add *textFont		[$tlc::theme setting font]	startupFile
		option add *Menu.font		[$tlc::theme setting boldfont]		startupFile
		option add *Menu.borderWidth				1	startupFile
	}
	"windows"	{
		option add *Radiobutton.borderWidth 	0	startupFile
		option add *Checkbutton.borderWidth 	0	startupFile
		option add *Entry.highlightThickness	0	startupFile
	}
}
#>>>

namespace eval tlc::Form {
	variable custom_types
	set custom_types(sql_lookup)	"tlc::form::Datavaultlookup"
}
namespace eval tlc::Pagedform {
	variable custom_types
	set custom_types(form)	"tlc::Pagedform::Form"
	set custom_types(html)	"tlc::Pagedform::Html"
}

namespace eval tlc {
	proc set_theme {name} {
		ttk::setTheme $name

		ttk::style map TLabel -foreground {invalid red}
	}

	# Try to choose a reasonable default
	apply {{} {
		switch -- [tk windowingsystem] {
			"win32" {
				set try	[list xpnative winnative clam]
			}

			"aqua" {
				set try	[list aqua clam]
			}

			default -
			"x11" {
				set try	[list clam]
			}
		}

		foreach theme $try {
			if {[catch {
				::tlc::set_theme $try
			} errmsg]} {
				#puts "Could not use theme: ($theme): $errmsg\n$::errorInfo"
			} else {
				break
			}
		}
	}}
}

