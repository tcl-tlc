#!/usr/bin/itkwish3.1

package require TLC
namespace import tlc::*
eval [go_home]

Log log -threshold debug

Domino #auto domfoo

proc changed {text} {
	log notice "changed: ($text)"
	$::domfoo tip
}

proc domchanged {} {
	log notice "domchanged"
}

$domfoo attach_output domchanged

mycombobox .cb -choices {foo bar bax} -cb changed
pack .cb

wm deiconify .

