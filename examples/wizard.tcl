#!/usr/bin/env tclsh8.5

# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

package require TLC 0.90.0

source "boilerplate.tcl"

Wizard .wizard -showhelp 1 -startpage start -routing {
	start { #<<<
		Next {
			{$dat(usefoo)}	get_foo
			default			get_bar
		}
	}
	#>>>
	get_foo { #<<<
		Back	start
		Next	get_bar
	}
	#>>>
	get_bar { #<<<
		Back {
			{$dat(usefoo)}	get_foo
			default			start
		}
		Next 	get_baz
	}
	#>>>
	get_baz { #<<<
		Skip_if	{$dat(usefoo)}
		Back	get_bar
		Next	review
	}
	#>>>
	review { #<<<
		Back	get_bar
		Finish	{}
	}
	#>>>
} -pages {
	start { #<<<
		title	"Hello, Wizard"
		type form
		schema {
			_layout		{row_args_sticky -resize none}
			_layout		{col_args_sticky -resize none}
			"Foo?"			{usefoo radiogroup -choices {
					"Use Foo"			1
					"Don't Use foo"		0
				}
			}

			_defaults {
				usefoo	1
			}

			_validation_not_blank {
				"%1 must be filled in"
				"Foo?"		usefoo
			}
		}
		help {
			<h2>This is the startpage of the wizard</h2>

			<p>Do you want some foo?  If you don't, you'll need to provide
			baz later on</p>
		}
	}
	#>>>
	get_foo { #<<<
		title	"Get Foo"
		type form
		schema {
			_layout		{row_args_sticky -resize none}
			_layout		{col_args_sticky -resize none}
			"Foo"			{foo entry}

			_validation_not_blank {
				"%1 must be filled in"
				"Foo"	foo
			}
		}
		help {
			Please provide the Foo.
		}
	}
	#>>>
	get_bar { #<<<
		type form
		schema {
			_layout		{row_args_sticky -resize none}
			_layout		{col_args_sticky -resize none}
			"Foo"			{foo label}
			"Bar"			{bar entry}

			_validation_not_blank {
				"%1 must be filled in"
				"Bar"	bar
			}

			_validation {
				{[info exists dat(usefoo)] && !$dat(usefoo) || [string first $dat(foo) $dat(bar)] > -1}
				"If use choose to use foo, bar must contain it"
				{bar}
			}
		}
	}
	#>>>
	get_baz { #<<<
		type form
		schema {
			_layout		{row_args_sticky -resize none}
			_layout		{col_args_sticky -resize none}
			"Baz"			{baz entry}

			_validation_not_blank {
				"%1 must be filled in"
				"Baz"	baz
			}
		}
	}
	#>>>
	review { #<<<
		#type form
		#schema {
		#	_layout		{row_args_sticky -resize none}
		#	_layout		{col_args_sticky -resize none}
		#	"Foo"			{foo label}
		#	"Bar"			{bar label}
		#}
		type html
		html {
			<table>
				<tr><td>Foo</td><td>%foo%</td></tr>
				<tr><td>Bar</td><td>%bar%</td></tr>
				<tr class="optional"><td>Bar</td><td>%bar%</td></tr>
			</table>
		}
		styles {
			.optional TD {
				font-style: italic;
				color: #303030;
			}
		}
	}
	#>>>
} -oncancel {
	exit 1
}

blt::table . \
		.wizard		1,1 -fill both

wm geometry . "600x320"

.wizard waitfor finished

array set dat	[.wizard get_data]
delete object .wizard

puts "Got:"
parray dat

exit
