set auto_path	[concat [file join [lrange [file split [pwd]] 0 end-1]] $auto_path]
if {[info exists no_tk]} {
	package require TLC-base
} else {
	package require TTLC
}
namespace import tlc::*
eval [go_home]

if {[file exists loglevels_class]} {
	tlc::Baselog::load_classmap loglevels_class
}
if {[file exists loglevels_func]} {
	tlc::Baselog::load_hotfuncs loglevels_func
}

proc start_console {} {
	update
	Console console -enable 1 -appname "tlc_test"
}
