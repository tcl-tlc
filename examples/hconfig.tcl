#!/usr/bin/tclsh

source "boilerplate.tcl"

wm withdraw .

package require Pg_sql

pg_sql::Pg_sql sql -dbname "gump"

Hconfig hcfg pg_sql [list sql "hconfig"]

hcfg save "users" {attribs {forex.class dealer}} {perms mergelist permset mergelist prefs mergearray prefset mergelist attribs mergearray attribset mergelist}
hcfg save "svcs" {} {perms mergelist permset mergelist prefs mergearray prefset mergelist attribs mergearray attribset mergelist}
array set uinfo [hcfg load "/users/nett/cyan"]
parray uinfo

exit
