#!/tcl8.4/bin/wish8.4

source "boilerplate.tcl"

proc onselect {key} {
	puts stderr "onselect: $key"
}

entry .cg1_manual -textvariable ::cg1
Checkgroup .cg1 -text "Group 1" -textvariable ::cg1 -mode "array" \
		-choices {Foo foo Bar bar Baz "hello world"}

entry .cg2_manual -textvariable ::cg2
Checkgroup .cg2 -text "Pick Any Two" -textvariable ::cg2 -mode "picklist" \
		-choices {Cheap cheap Fast fast Reliable reliable} -orient h
array set picktwo {}
Gate #auto picktwo(cheap_fast) -mode and
Gate #auto picktwo(cheap_reliable) -mode and
Gate #auto picktwo(fast_reliable) -mode and

$picktwo(cheap_fast) attach_input [.cg2 signal_ref option_cheap]
$picktwo(cheap_fast) attach_input [.cg2 signal_ref option_fast]

$picktwo(cheap_reliable) attach_input [.cg2 signal_ref option_cheap]
$picktwo(cheap_reliable) attach_input [.cg2 signal_ref option_reliable]

$picktwo(fast_reliable) attach_input [.cg2 signal_ref option_fast]
$picktwo(fast_reliable) attach_input [.cg2 signal_ref option_reliable]

.cg2 attach_signal cheap $picktwo(fast_reliable) inverted
.cg2 attach_signal fast $picktwo(cheap_reliable) inverted
.cg2 attach_signal reliable $picktwo(cheap_fast) inverted

.cg1 register_handler onselect onselect

.cg1 set_tips \
		foo		"This is a foo" \
		bar		"Choose a bar instead"

Form .form -schema {
	"Foo"	{foo entry}
	"Bar"	{foo checkgroup -choices {"Option 1" 1 "Option 2" 2}}
}

table . -padx 5 -pady 5 \
		.cg1_manual	1,1 -fill x \
		.cg1		2,1	-fill both \
		.cg2_manual	3,1 -fill x \
		.cg2		4,1	-fill both \
		.form		5,1 -fill both

[Signal_monitor .cg1_monitor -signal_source .cg1] show
[Signal_monitor .cg2_monitor -signal_source .cg2] show
