#!/usr/bin/env tclsh8.4

source "boilerplate.tcl"

proc cb {foo} {
	puts "foo: ($foo)"
	.e configure -state disabled
}


proc bar {newdate} {
	puts "newdate: ($newdate)"
	puts "foo: ($::foo)"
}

Signal #auto bar
$bar set_state 1
option add *stateGate $bar
Dateentry .e -textvariable ::foo -year 2006 -month Jul -day 07 -command bar -resultformat "%d %b %Y"
#.e attach_signal $bar
#set foo		"kjasdhf"

#after 3000 [list $bar set_state 0]

pack .e

wm deiconify .

start_console
