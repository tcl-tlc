#!/usr/bin/itkwish3.1

package require TLC
namespace import tlc::*
eval [go_home]

Log log -threshold debug

proc list_holidays {y m} {
	if {$y == 2003 && $m == 06} {
		return {13 "USD Bank holiday" 17 "ZAR Bank holiday"}
	}

	return {}
}


proc newsel {date} {
	log notice "New date selected: ($date)"
}


proc newdatesel {newdate} {
	log notice "New datesel: ($newdate)"
}


Calendar .cal -month 6 -validfrom "2003-06-12" -validto "2003-07-24" \
		-special_cb list_holidays -command newsel
Datelookup .calbut -newselection newdatesel
pack .cal .calbut
wm deiconify .

#after 1000 {.cal configure -fillerbg blue}
#after 1000 {.cal configure -weekdayheaderbg blue}
#after 1000 {.cal configure -availablebg blue}
#after 1000 {.cal configure -invalidbg blue}
#after 1000 {.cal configure -weekendbg blue}
#after 1000 {.cal configure -specialbg blue}
#after 1000 {.cal configure -dayfont {Helvetica -14 bold}}
#after 1000 {.cal configure -weekdayheaderfont {Helvetica -14 bold}}
#after 1000 {.cal configure -weekdayheaderfg red}
#after 1000 {.cal configure -availablefg red}
#after 1000 {.cal configure -invalidfg red}
#after 1000 {.cal configure -weekendfg red}
#after 1000 {.cal configure -specialfg red}
#after 1000 {.cal configure -borders red}
#after 3000 {.cal configure -validfrom "" -validto ""}
#after 4000 {.cal scroll_month -1}
#after 5000 {.cal scroll_month +2}
#after 6000 {.cal scroll_year -1}
#after 7000 {.cal scroll_year +2}


