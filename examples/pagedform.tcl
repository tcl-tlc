#!/usr/bin/env tclsh8.5

# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

package require Tcl 8.5
package require TLC
eval [tlc::go_home]

set loglevels_class	"loglevels_class_[file tail [info script]]"
if {[file exists $loglevels_class]} {
	puts "loading classmap: ($loglevels_class)"
	tlc::Baselog::load_classmap $loglevels_class
}

. configure -background [ttk::style lookup $::ttk::currentTheme -background]
wm geometry . "450x250"

tlc::Pagedform .p -mode list -input_transform {
	if {![info exists dat(foo)] || $dat(foo) eq ""} {
		set dat(foo)	"Initial foo"
	}
	set dat(fake1)	$dat(foo)
	set dat(fake2)	$dat(foo)
} -output_transform {
	array unset dat fake2
} -form_schema_prefix {
	_layout		{row_args_sticky -resize none}
	_layout		{col_args_sticky -resize none}
} -pages {
	"Page 1" { #<<<
		type html
		html {
			Foo: &quot;%foo%&quot;<br>
			Bar: &quot;%bar%&quot;<br>
			Baz: &quot;%baz%&quot;<br>
		}
		styles {
		}
	}
	#>>>
	"Page 2" { #<<<
		type form
		disabled_if {
			$dat(baz) eq $dat(foo)
		}
		options	{-padding 2}
		schema {
			"Foo"	{foo}
			"Bar"	{bar}
		}
	}
	#>>>
	"Page 3" { #<<<
		options	{-padding 2}
		schema {
			"Baz"	{baz}
			"Quux"	{bar label}

			_defaults {
				baz		"Baz from _defaults"
			}

			_validation_not_blank {
				"%1 must not be blank"
				"Quux"		bar
			}
		}
	}
	#>>>
	"This is a very long page title" { #<<<
		options {-winding h}
		disabled_if {
			$dat(blinken)
		}
		schema {
			"A" {a entry -width 4}
			"B" {b entry -width 4}
			"C" {c entry -width 4}
			_defaults {
				blinken	0
			}
		}
	}
	#>>>
}

.p set_data {
	bar		"Hello bar"
	b		"this is b"
	baz		"Updated baz"
}

proc blink {} {
	set current	[dict get [.p get_data] blinken]
	if {$current} {
		.p set_data blinken 0
	} else {
		.p set_data blinken 1
	}
	after 1000 blink
}

proc submit {} {
	array set dat	[.p get_data]
	puts "dat:"
	parray dat
}

tlc::Tools .ops
.ops add "Submit" submit right
.ops add "Close" exit right

.ops attach_signal "Submit" [.p signal_ref valid]

blt::table . \
		.p		1,1 -fill both \
		.ops	2,1 -fill x -pady {5 0}
blt::table configure . r2 -resize none

blink
