#!/usr/bin/itkwish

source "boilerplate.tcl"

Formdialog .f -title "Formdialog title" -preface "
This is a test of the Formdialog widget.  It's purpose is to optimize the process of gathering form style data from the user inline in logic code, in situations where Getinf is not sufficient
"

set res(foo)	"Foobar!"

if {[.f ask res "Foo" foo "Bar" {bar checkbutton -onvalue "x" -offvalue "y"} _validation {{$dat(foo) == "Foo"} "Foo must be \"Foo\"" {foo}}]} {
	puts "Ok:"
	parray res
} else {
	puts "cancelled"
}

exit
