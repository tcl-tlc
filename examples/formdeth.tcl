#!/usr/bin/tclsh

set auto_path	[concat [file join [lrange [file split [pwd]] 0 end-1]] $auto_path]

package require TLC 0.54.1
namespace import tlc::*
eval [go_home]

Form .form -schema {
	"Foo"	foo
	"Bar"	bar
}

blt::table . -padx 5 -pady 5 \
		.form		1,1 -fill both

destroy .form
#delete object .form

update idletasks

puts "children of the .:"
puts [join [winfo children .] \n]

puts "all objects:"
puts [find objects]


Form .form -schema {
	_layout	{default_options entry -relief raised -background red}
	"Foo"	foo
	"Bar"	bar
}

blt::table . -padx 5 -pady 5 \
		.form		1,1 -fill both

