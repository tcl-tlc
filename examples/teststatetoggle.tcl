#!/usr/bin/itkwish3.1

source "boilerplate.tcl"

Signal #auto flipflop

array set toggles {}

set textvar		"hello"
entry .e -textvariable textvar
StateToggle #auto toggles(entry1) .e \
		-state {disabled normal} \
		-background {red blue}

$toggles(entry1) attach_signal $flipflop

table . \
		.e		1,1

proc doflip {} {
	$::flipflop toggle_state
	after 3000 doflip
}

doflip

