#!/usr/bin/env tclsh

source "boilerplate.tcl"

wm withdraw .

Getinf .getinf

set foo		"Initial value of foo"

if {[.getinf ask "Foo" foo "Bar" {bar checkbutton}]} {
	puts "Got foo: ($foo) bar: ($bar)"
} else {
	puts "User cancelled"
}

exit
