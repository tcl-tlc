#!/usr/bin/wish

package require TLC
namespace import tlc::*
eval [go_home]

wm deiconify .

checkbutton .i1 -text "i1" -command [list seti i1] -variable i1
checkbutton .i2 -text "i2" -command [list seti i2] -variable i2
checkbutton .i3 -text "i3i" -command [list seti i3] -variable i3
checkbutton .i4 -text "i4" -command [list seti i4] -variable i4

checkbutton .o -text "output" -variable o

proc seti {line} {
	upvar ${line}_o obj
	$obj set_state [set ::${line}]
}

proc linechange {linestate} {
	set ::o	$linestate
}


Signal #auto i1_o
Signal #auto i2_o
Signal #auto i3_o
Signal #auto i4_o

Gate #auto logic -mode and
if {0} {
	$logic attach_input $i1_o
	$logic attach_input $i2_o
	$logic attach_input $i3_o inverted
	$logic attach_input $i4_o
} else {
	$logic attach_var_input ::i1
	$logic attach_var_input ::i2
	$logic attach_var_input ::i3 inverted
	$logic attach_var_input ::i4
}

$logic attach_output ::linechange


table . -padx 5 -pady 5 \
	.i1		1,1 \
	.i2		2,1 \
	.i3		3,1 \
	.i4		4,1 \
	.o		1,2 -rspan 4

