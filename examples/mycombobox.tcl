#!/usr/bin/itkwish3.1

source "boilerplate.tcl"

mycombobox .foo -choices {
	"USD"
	"ZAR"
	"EUR"
	"GBP"
	"JPY"
}

Tools .tools
.tools add "Use USD" {puts "use USD"}
.tools add "Use ZAR" {puts "use ZAR"}
.tools add "Use EUR" {puts "use EUR"}
.tools add "Use GBP" {puts "use GBP"}
.tools add "Use JPY" {puts "use JPY"}
.tools add "Has a U" {puts "has a U"}
.tools add "Close" {exit} right

.tools attach_signal "Use USD" [.foo if_val "USD"]
.tools attach_signal "Use ZAR" [.foo if_val "ZAR"]
.tools attach_signal "Use EUR" [.foo if_val "EUR"]
.tools attach_signal "Use GBP" [.foo if_val "GBP"]
.tools attach_signal "Use JPY" [.foo if_val "JPY"]

.tools attach_signal "Has a U" [.foo if_expr {[string first "U" "%v"] != -1}]


table . -padx 5 -pady 5 \
		.foo		1,1 \
		.tools		2,1 -fill x

