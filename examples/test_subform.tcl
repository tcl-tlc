#!/usr/bin/env tclsh

source "boilerplate.tcl"

Form .form -schema {
	"Foo"	{foo}
	"Bar"	{bar}
	"Baz"	{baz subform -framerelief groove -frameborderwidth 2 -schema {
		"Test"	{test}
		"RG"	{test radiogroup -choices {"Option 1" 1 "Option 2" 2}}
	}}
}

Tools .tools
.tools add "Dump" {puts [.form get_data]} right
.tools add "Close" exit right

blt::table . -padx 5 -pady 5 \
		.form		1,1 \
		.tools		2,1 -fill x
blt::table configure . r2 -resize none
