#!/usr/bin/env tclsh8.5

package require Tcl 8.5
package require TLC

if {[file exists loglevels_class_formlist]} {
	tlc::Baselog::load_classmap loglevels_class_formlist
}

tlc::Form .f -schema {
	"This is a list of things"	{things
		list \
				-headers		Thing \
				-allow_reorder	1 \
				-add_label		"New thing" \
				-showheader		0 \
				-add_schema		{entry} \
				-add_schema_suffix	{
			_validation {
				{[string is digit -strict $dat(newvalue)]}
				"New thing must be a number"
				{newvalue}
			}
		}
	}
}

tlc::Tools .ops
.ops add "Ok" {
	array set dat	[.f get_data]
	parray dat
	exit 0
} right

blt::table . \
		.f		1,1 \
		.ops	2,1 -fill x -pady {5 0}
blt::table configure . r2 -resize none
