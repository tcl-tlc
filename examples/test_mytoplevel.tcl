#!/usr/bin/tclsh

source "boilerplate.tcl"

set t	[tlc::Mytoplevel .t]

destroy .t

puts "children of the .:"
puts [join [winfo children .] \n]

puts "objects:"
puts [join [find objects] \n]
