#!/usr/bin/tclsh

set auto_path	[concat [list ..] $auto_path]

package require TLC 0.45

wm withdraw .

namespace eval crypto {
	variable devmode	1
}

package require Authenticator
authenticator::Authenticator auth -ip localhost -pbkey authenticator.pub

auth waitfor login_allowed

if {![auth login "cyan@nett" "foo"]} {
	if {![auth login "user@cf" "user"]} {
		puts stderr "Cannot login: [auth last_login_message]"
		exit -1
	}
}

set conn	[auth connect_svc "test"]

tlc::DSchan ds -connector $conn -tag "testds"
tlc::Datasource_filter dsf -ds ::ds

set tlc::Baselog::classmap(::tlc::DSchan)	10

tlc::Browse_tktreectrl .list -datasource dsf

pack .list -fill both -expand true

wm deiconify .
