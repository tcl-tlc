#!/usr/bin/itkwish

source "boilerplate.tcl"

set strict	0
set policy	""

proc new_policy {text} {
	puts "text: ($text)"
	set ::policy	$text

	reassess
}

proc reassess {} {
	if {$::strict} {
		set ::policy	[string toupper $::policy]
	} else {
		set ::policy	[string tolower $::policy]
	}

	.te configure -policy $::policy
}

Tagentry .te -tagtext "%"

checkbutton .strict -variable ::strict -command {reassess} -text "strict"

Mycombobox .c -choices {
	alnum alpha ascii boolean control digit double false graph integer lower
	print punct space true upper wordchar xdigit "" invalidoption
} -cb new_policy

table . \
		.te		1,1 -anchor w \
		.strict	2,1 -anchor w \
		.c		3,1 -anchor w

wm deiconify .
