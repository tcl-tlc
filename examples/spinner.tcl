#!/usr/bin/env tclsh8.4

source "boilerplate.tcl"

entry .e
Spinner .spinner -choices {Foo Bar Baz} -textvariable ::s

blt::table . \
		.e				1,1 \
		.spinner		2,1

start_console

