#!/usr/bin/tclsh

package require Tktable
rename table tktable

source "boilerplate.tcl"

proc row_added_notify {row} {
	puts "Row added: ($row)"
	return [list qty 0]
}

proc row_removed_notify {row olddata} {
	array set tmp	$olddata
	puts "Row removed ($row), was:"
	parray tmp
}

proc item_selected_changed {newstate} {
	puts "item_selected_changed: ($newstate)"
}

tlc::Tablelist .tl -allow_reorder 0 -schema {
	_layout				{table_args -borderwidth {1 0 1 0} -relief solid}
	_layout				{col_width -130}
	"Packaging"			{packaging spinner -choices {
		"1 Kg Bottle"		
		"500 g Bottle"
		"250 g Bottle"
		"Loose Granules"
	}}
	_layout				{col_width -40}
	_layout				{col_args -anchor e}
	"Qty"				{qty}
	"UoM"				{uom}

	_layout				{row_args_even -background #f8f8f8}
	_layout				{row_args_odd -background #e0e0e0}

	_set_tags {
		title 	{
			-background #b2b2b2
			-foreground black
			-relief raised
			-borderwidth 1
			-anchor w
		}
		test	{
			-background blue
			-foreground black
			-relief sunken
			-borderwidth 1
			-anchor w
		}
	}	

	_validation {
		{[string is double -strict $dat(qty)] && $dat(qty) > 0}
		"Quantity must be more than 0"
		{qty}
	}
}

.tl set_rows {
	packaging "1 Kg Bottle"		qty 2
} {	packaging "500 g Bottle"	qty 1
} { packaging "250 g Bottle"	qty 6
}

#.tl set_tags \
#	rowselect {
#		-background yellow
#		-foreground black
#		-relief sunken
#		-borderwidth 1
#	}

puts "get_rows:\n[.tl get_rows]"

.tl register_handler row_added [list row_added_notify]
.tl register_handler row_removed [list row_removed_notify]
[.tl signal_ref item_selected] attach_output [list item_selected_changed]

Form .form -schema {
	_layout			{cell_args -fill both}
	"Hello"		{
		t tablelist -schema {
			"Foo"	{foo}
			"Bar"	{bar}
			"Baz"	{baz}

			#_layout				{row_args_even -background #f8f8f8}
			#_layout				{row_args_odd -background #e0e0e0}
		}
	}
}

Tools .tools
.tools add "Dump" {puts [.tl get_rows]} right
.tools add "Close" {exit} right

blt::table . -padx 5 -pady 5 \
		.tl			1,1 -fill both \
		.form		2,1 -fill both \
		.tools		3,1 -fill x
blt::table configure . r3 -resize none

