#!/usr/bin/env tclsh8.4

package require TLC
namespace import tlc::*
eval [go_home]


proc choose {which} {
	puts "chose: ($which)"
}


Mymenu .m -schema {
	"Option 1"		{command {choose "o1"}}
	"Option 2"		{command {choose "o2"}}
	"Option 3"		{command {choose "o3"}}
	"Other" {cascade {
		"Option 4"		{command {choose "o4"}}
		"Option 5"		{command {choose "o5"}}
		"Still Others" {cascade {
			"Option 6"		{command {choose "o6"}}
			"Option 7"		{command {choose "o7"}}
		}}
	}}
}


bind . <Button-3> [list tk_popup .m %X %Y]
