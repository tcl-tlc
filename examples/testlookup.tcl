#!/usr/bin/itkwish3.1

source "boilerplate.tcl"

proc l {id name} {
}

proc ne {} {
	$::enabled set_state $::gui_enabled
}

checkbutton .e -text "enabled" -variable gui_enabled -command ne

Signal #auto enabled

Lookup .l -lookup_command l
.l attach_signal $enabled

Form .f -schema {
	"Test"	{t lookup}
}
[.f path "Test"] attach_signal $enabled

pack .e .l .f -side top -anchor nw

wm deiconify .
