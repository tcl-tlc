#!/usr/bin/itkwish
#package require -exact Itcl 3.1
#package require -exact Itk 3.1
#namespace import -force itcl::*
#namespace import -force itk::*

source "boilerplate.tcl"

package require Pg_sql

pg_sql::Pg_sql sql -dbname "testnett" -user ""

proc bgerror {args} {
	puts $::errorInfo
}

proc onselect_id {row_id} {
	puts "Got onselect_id: ($row_id)"

	set inf		[list id $row_id]
	set tmp		[sql getlist "
		select
			data,
			path
		from
			hconfig
		where
			id = '[sql quote $row_id]'
	"]
	lappend inf		data [lindex [lindex $tmp 0] 0] \
						path [lindex [lindex $tmp 0] 1]

	.f.details set_data $inf

}


proc dump_selection {} {
	puts "current selection: ([.f.bt get_selected_items])"
}

set ds [tlc::Datasource_sql ::#auto -sql_obj sql \
	-criteria {
		Path {path combobox -choices {bank user} -initial_choice bank}
		Variables {data}
		"Require match on all criteria" {boolean checkbox}
	} \
	-lookup_query "select
						id,
						path,
						leaf,
						datatypes
					from
						hconfig
					where
						path like '%path%%'
						%boolean%
						data like '%%data%%'
					order by path;" \
	-insert_query "insert into hconfig (
						path,
						leaf,
						datatypes
					) values (
						'%path%',
						%leaf%,
						'%datatypes%'
					);" \
	-update_query "update hconfig set
						path='%path%',
						leaf=%leaf%,
						datatypes='%datatypes%'
					where
						id=%id%;" \
	-delete_query "delete 
						from hconfig
					where
						id=%id%;" \
	-item_schema {
					Path {path}
					Variables {data}
					"Require match on all criteria" {boolean checkbox}
					Leafnode {leaf checkbox}
					Datatypes {datatypes}
				} \
	-id_column 0 \
	-full_row_query "select
						id,
						path,
						data,
						leaf,
						datatypes
					from
						hconfig
					where
						id=%id%"
	]

$ds set_criteria_values "path user boolean or data {}"
$ds set_criteria_map "boolean {1 and 0 or}"
frame .f -relief groove -borderwidth 4

set tv [tlc::Browse_treeview_flat .f.bt \
		-datasource $ds \
		-show_criteria 1 \
		-criteriapos	{9,10 -pady {0 15} -anchor e -padx {5 25}} \
		-filterpos		{8,10 -pady {0 15} -anchor e -padx {5 25}}\
		-filter_insensitive 1\
		-filter_mode 1 \
		-tree_style web]
set sr [$tv selected_ref]
$tv action_add "Print" print
$tv action_attach_signal "Print" $sr
$tv action_add_supported
$tv filter_add "Path" path {[string first $filter(path) $row(path)] > -1}
$tv filter_add_standard "Data Types" datatypes {match_left}
#$tv filter_add "Path2" {path2} {1}

Form .f.details -schema {
	"ID"		{id	  label}
	"Path"		{path label}
	"Data"		{data text}
}
.f.details attach_signal [.f.bt selected_ref]

table .f \
		.f.bt			1,1 -fill both \
		.f.details		1,2
table configure .f c2 -resize none

.f.bt register_handler onselect_id			onselect_id


button .exit -command "destroy ." -text "Exit"
button .style -command "$tv style_tree web"
table . \
	.f 		1,1 -fill both \
	.style	2,1 -anchor w \
	.exit	2,1 -anchor e
table configure . c2 r2 -resize none

bind . <Escape> "destroy ."
. configure -height 480 -width 640
Confirm .confirm
