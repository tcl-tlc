#!/usr/bin/itkwish3.1

source "boilerplate.tcl"

proc link_handler {rest args} {
	array set a $args
	puts "args:"
	parray a
	switch -- $rest {
		"hello" {
			puts "Hello world"
		}
		"exit" {
			exit
		}
	}
}

tlc::uri register_handler test link_handler

Label_link .l1 -text "Hello" -target "test://hello?foo=bar&baz=quux"
Label_link .l2 -text "Quit" -target "test://exit"

table . -padx 5 -pady 5 \
		.l1		1,1 \
		.l2		2,1
