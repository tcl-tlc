#!/usr/bin/env tclsh8.4

source "boilerplate.tcl"

package require TLC 0.77.0
option add *Entry.background			[$tlc::theme setting textbackground]
option add *Entry.highlightBackground	[$tlc::theme setting background]

set data	{}
lappend data	{asdf lkjqh lkqjwehr}
lappend data	{agwer sdfg jkh}
lappend data	{asdfg kjqew kjhvawe}

set ds	[DSlist #auto -headers {foo bar baz} -list $data]

proc manage_bar_visibility {visible} {
	.l column_configure bar -visible $visible
}

proc add {} {
	puts "Add called"
}

proc edit {} {
	puts "Edit called, on item(s) ([.l get_selected_items])"
}

proc del {} {
	puts "Del called, on item(s) ([.l get_selected_items])"
}

Browse_tktreectrl .l -datasource $ds -actionside right -actionbuttonwidth 5 \
	   	-column_options {
	foo		{-expand false -text "Foo ID"}
	bar		{-visible false -justify right}
}
[.l component filters] register_handler onchange,bleh manage_bar_visibility

.l action_add "Add" add
.l action_add "Edit" edit
.l action_add "Del" del

.l action_attach_signal "Edit" [.l signal_ref item_selected]
.l action_attach_signal "Del" [.l signal_ref item_selected]

.l filter_add "Search" {spec entry -width 8} {
	[string match -nocase "*$filter(spec)*" $row(foo)] ||
	[string match -nocase "*$filter(spec)*" $row(baz)]
}
.l filter_add " Bleh" {bleh checkbutton -text "Show Bar"}

proc onselect_id {newid} {
	puts "newid: ($newid)"
}

proc onselect_row {newrow} {
	puts "newrow: ($newrow)"
}

proc item_selected_changed {selected} {
	puts "itemselected: ($selected)"
}

.l register_handler onselect_id onselect_id
.l register_handler onselect onselect_row
[.l signal_ref item_selected] attach_output item_selected_changed

blt::table . \
		.l		1,1 -fill both
