#!/usr/bin/env tclsh8.5

source "boilerplate.tcl"

proc onselect {key} {
	puts stderr "onselect: $key"
}

set ::rg1	bar
entry .rg1_manual -textvariable ::rg1
Radiogroup .rg1 -text "Group 1" -textvariable ::rg1 \
		-choices {Foo foo Bar bar Baz "hello world"}

entry .rg2_manual -textvariable ::rg2
Radiogroup .rg2 -text "Group 2" -textvariable ::rg2 \
		-choices {"Option 1" 1 "Option 2" bar} -orient h

entry .rg3_manual -textvariable ::rg3
Radiogroup .rg3 -text "Group 3" -textvariable ::rg3 -state disabled \
		-choices {"Option 1" 1 "Option 2" 2} -orient h -initial_value 2

.rg1 register_handler onselect onselect

.rg1 set_tips \
		foo		"This is a foo" \
		bar		"Choose a bar instead"

Form .form -schema {
	"Foo"	{foo entry}
	"Bar"	{foo radiogroup -choices {"Option 1" 1 "Option 2" 2}}
	"Baz"	{sf subform -winding h -schema {
		"in subform"	{baz radiogroup -choices {"A" a "B" b}}
	}}
}
.form set_data foo 1
.form set_data sf [list baz a]

table . -padx 5 -pady 5 \
		.rg1_manual	1,1 -anchor w \
		.rg1		2,1	-fill both \
		.rg2_manual	3,1 -anchor w \
		.rg2		4,1	-fill both \
		.rg3_manual	5,1 -anchor w \
		.rg3		6,1	-fill both \
		.form		7,1 -fill both

#[Signal_monitor .rg1_monitor -signal_source .rg1] show
#[Signal_monitor .rg2_monitor -signal_source .rg2] show
[Signal_monitor .rg3_monitor -signal_source .rg3] show

