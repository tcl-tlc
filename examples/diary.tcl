#!/tcl8.4/bin/wish8.4

package require TLC
namespace import tlc::*


tlc::Diary .diary

Tools .tools
.tools add "Close" {exit} right

table . -padx {3 3} -pady {3 3} \
		.diary	1,1 -fill both \
		.tools	2,1 -fill x 
