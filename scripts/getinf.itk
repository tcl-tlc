# vim: foldmarker=<<<,>>>

proc tlc::getinf {pathName args} {
	uplevel tlc::Getinf $pathName $args
}


class tlc::Getinf {
	inherit tlc::Modal

	constructor {args} {}

	public {
		method ask {args}
	}

	private {
		variable data
	}
}


body tlc::Getinf::constructor {args} { #<<<1
	eval itk_initialize $args

	option add *$wdb*Button.width				12	widgetDefault
	option add *$wdb*Button.highlightThickness	1
	option add *$wdb*Button.takeFocus			1
	
	frame $w.args

	tlc::Tools $w.tools
	$w.tools add "OK" [code $this choose 1] right
	$w.tools add "Cancel" [code $this choose 0] right

	blt::table $w -padx 5 -pady 5 \
		$w.args		1,1 -fill x \
		$w.tools	2,1 -pady {8 0} -fill x
	blt::table configure $w r2 -resize none
}


body tlc::Getinf::ask {args} { #<<<1
	catch {eval destroy [winfo children $w.args]}
	catch {unset data}
	array set data {}
	
	set row		1
	foreach {label varspec} $args {
		set varname		""
		set type		""
		set default		""
		foreach {varname type default} $varspec {break}
		set rest		[lrange $varspec 3 end]
		
		upvar $varname dest_$varname
		if {[info exists dest_$varname]} {
			set data($varname)		[set dest_$varname]
		} else {
			set data($varname)		$default
		}

		label $w.args.$row,l -text "${label}:"

		switch $type {
			"" -
			entry {
				entry $w.args.$row,v -textvariable [scope data($varname)]
			}

			checkbox -
			checkbutton {
				checkbutton $w.args.$row,v -variable [scope data($varname)]
			}

			label {
				label $w.args.$row,v -text $data($varname) -justify left \
					-font [tlc::default_config get boldfont]
			}

			textbox -
			text {
				tlc::vartextbox $w.args.$row,v \
						-textvariable [scope data($varname)]
			}

			button {
				button $w.args.$row,v -textvariable [scope data($varname)] \
					-command [lindex $rest 0]
			}

			combobox -
			mycombobox {
				tlc::mycombobox $w.args.$row,v \
						-textvariable [scope data($varname)] \
						-choices [lindex $rest 0]
			}
			
			default {
				puts stderr "Unknown type: ($type)"
			}
		}

		blt::table $w.args \
			$w.args.$row,l		$row,1 -anchor ne \
			$w.args.$row,v		$row,2 -anchor nw
		blt::table configure $w.args c1 -resize none

		incr row
	}
	focus $w.args.1,v

	waitresult

	if {$result} {
		foreach {label varspec} $args {
			set varname		""
			set type		""
			foreach {varname type} $varspec {break}
			set dest_$varname	$data($varname)
		}
	}
	
	return $result
}


