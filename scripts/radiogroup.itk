# vim: foldmarker=<<<,>>>

# Signals fired:
#	onselect(value)		- Fired when a radio button is selected

proc tlc::radiogroup {pathname args} {
	uplevel [list tlc::Radiogroup $pathname] $args
}


class tlc::Radiogroup {
	inherit tlc::Mywidget tlc::Handlers tlc::Signalsource \
			tlc::Tooltips tlc::Textvariable

	constructor {args} {}
	destructor {}

	itk_option define -foreground foreground Foreground black need_rerender
	itk_option define -disabledforeground disabledForeground Foreground grey need_rerender
	itk_option define -pady padY PadY 0
	itk_option define -borderwidth borderWidth BorderWidth 2 need_rerender
	itk_option define -relief relief Relief "groove" need_rerender
	itk_option define -text text Text "" need_rerender
	itk_option define -labelwidget labelWidget LabelWidget "" need_rerender
	itk_option define -labelanchor labelAnchor LabelAnchor "" need_rerender

	public {
		variable state			"normal"
		variable choices 		{}			need_rerender
		variable orient			"v"			need_rerender
		variable cell_args		"-anchor w"	need_rerender
		variable initial_value
		variable value			""
		variable wrap_len		""
		variable text			""

		method domino_ref {domino}
		method attach_signal {key signal {sense normal}}
		method path {key}
	}

	protected {
		method textvariable_changed {newvalue}
	}

	private {
		variable dominos
		variable toggles
		variable keymap
		variable base
		variable afterid	""

		method need_rerender {} {$dominos(need_rerender) tip}
		method rerender {}
		method option_changed {key newvalue}
		method value_changed {}
		method update_value {val}
	}
}


configbody tlc::Radiogroup::state { #<<<1
	[stategate_ref] configure -default [expr {$state eq "normal"}]
}


configbody tlc::Radiogroup::pady { #<<<1
	#foreach child [winfo children $w] {
	#	$child configure -pady $itk_option(-pady)
	#}
}


body tlc::Radiogroup::constructor {args} { #<<<1
	log debug

	array set dominos	{}
	array set toggles	{}
	array set keymap	{}

	tlc::Domino #auto dominos(need_rerender) -name "$w need_rerender"
	tlc::Domino #auto dominos(value_changed) -name "$w value_changed"
	tlc::Gate #auto signals(item_selected) -name "$w item_selected" \
			-mode "or" -default 0

	set base	$w.border.inner

	$dominos(need_rerender) attach_output [code $this rerender]

	itk_initialize {*}$args

	#if {0 && [$dominos(need_rerender) pending]} {
	#	log debug "forcing rerender in constructor"
	#	$dominos(need_rerender) tip_now
	#}

	$dominos(need_rerender) force_if_pending

	# Nasty hack to allow -initial_value <<<
	array set tmp	$args
	if {[info exists tmp(-initial_value)]} {
		set_textvariable $initial_value
		textvariable_changed $initial_value
		#value_changed
	}
	# Nasty hack to allow -initial_value >>>
}


body tlc::Radiogroup::rerender {} { #<<<1
	log debug $w
	if {[winfo exists $w.border]} {
		destroy $w.border
	}
	if {$itk_option(-text) ne "" || $itk_option(-labelwidget) ne ""} {
		set options	{}
		foreach option {text labelanchor labelwidget} {
			if {$itk_option(-$option) ne ""} {
				lappend options -$option $itk_option(-$option)
			}
		}
		ttk::labelframe $w.border \
				-borderwidth	$itk_option(-borderwidth) \
				-relief			$itk_option(-relief) \
				{*}$options
	} else {
		ttk::frame $w.border \
				-borderwidth	$itk_option(-borderwidth) \
				-relief			$itk_option(-relief)
	}
	pack $w.border -fill both -expand true

	foreach key [array names toggles] {
		array unset toggles $key
	}
	array unset keymap
	array set keymap	{}
	foreach signal [array names signals option_*] {
		array unset signals($signal)
	}
	ttk::frame $w.border.inner
	pack $w.border.inner -fill both -expand true

	set seq	0
	foreach {name key} $choices {
		if {$wrap_len ne ""} {
			set major	[expr {$seq % $wrap_len}]
			set minor	[expr {$seq / int($wrap_len)}]
			if {$orient eq "h"} {
				set r	$minor
				set c	$major
			} else {
				set r	$major
				set c	$minor
			}
		} else {
			if {$orient eq "h"} {
				set c	$seq
				set r	0
			} else {
				set c	0
				set r	$seq
			}
		}

		ttk::radiobutton $base.$c,$r -text "$name" \
				-value $key \
				-variable [scope value] \
				-command [code $this value_changed]

		tlc::StateToggle #auto toggles($c,$r) $base.$c,$r \
				-state {disabled normal}
		$toggles($c,$r) attach_input [stategate_ref]

		tlc::Signal #auto signals(option_$key) -name "$w option $key selected"
		$signals(item_selected) attach_input $signals(option_$key)
		$signals(option_$key) attach_output [code $this option_changed $key]

		set keymap($key)	"$c,$r"

		blt::table $base $base.$c,$r $r,$c \
				{*}$cell_args

		incr seq
	}
}


body tlc::Radiogroup::destructor {} { #<<<1
	log debug
	array unset toggles
	array unset dominos
	after cancel $afterid; set afterid	""
}


body tlc::Radiogroup::attach_signal {key signal {sense normal}} { #<<<1
	if {![info exists keymap($key)]} {error "No such key: ($key)"}

	return [$toggles($keymap($key)) attach_input $signal $sense]
}


body tlc::Radiogroup::path {key} { #<<<1
	if {[$dominos(need_rerender) pending]} {
		log debug "forcing rerender to answer path request"
		$dominos(need_rerender) tip_now
	}

	if {![info exists keymap($key)]} {error "No such key: ($key)"}

	return "$base.$keymap($key)"
}


body tlc::Radiogroup::option_changed {key newvalue} { #<<<1
	log debug
	if {$newvalue} {
		invoke_handlers onselect $key
	}
}


body tlc::Radiogroup::value_changed {} { #<<<1
	log debug "setting textvariable to ($value)"
	set_textvariable	$value

	foreach key [array names keymap] {
		$signals(option_$key) set_state [expr {$value eq $key}]
	}

	$dominos(value_changed) tip
}


body tlc::Radiogroup::textvariable_changed {newvalue} { #<<<1
	log debug
	set afterid	[after idle [code $this update_value $newvalue]]
}


body tlc::Radiogroup::update_value {val} { #<<<1
	set value	$val

	foreach key [array names keymap] {
		$signals(option_$key) set_state [expr {$value eq $key}]
	}

	$dominos(value_changed) tip
	log debug "value now: ($value)"
}


body tlc::Radiogroup::domino_ref {domino} { #<<<1
	if {![info exists dominos($domino)]} {
		error "Invalid domino: \"$domino\", choose from [array names dominos]" "" \
				[list invalid_domino $domino]
	}
	return $dominos($domino)
}


