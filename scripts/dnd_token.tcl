proc tlc::dnd_token {pathName args} {
	if {$::tcl_platform(platform) == "windows"} {
		set top	[toplevel $pathName.token]
		wm withdraw $top
	} else {
		return [uplevel blt::dnd token window $pathName \
				-activebackground red \
				-borderwidth 1 \
				-relief raised \
				-activerelief raised \
				-anchor s \
				$args]
	}
}


