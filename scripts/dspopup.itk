# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

option add *DSpopup.showRoot			false		widgetDefault
option add *DSpopup.yScrollMode			"static"	widgetDefault
option add *DSpopup.xScrollMode			"dynamic"	widgetDefault
option add *DSpopup.actionButtonWidth	9			widgetDefault


class tlc:DSpopup {
	inherit tlc::Module tlc::Baselog

	constructor {args} {}
	destructor {}

	public {
		variable ds		{}
		variable parent
	}

	private {
		variable old_ds
		variable row_cache	{}

		method headers_changed {new_headers}
		method item_selected {item_arr}
		method onchange {}
	}
}


configbody tlc::DSpopup::ds { #<<<1
	if {[info exists old_ds] && [itcl::is object $old_ds]} {
		$old_ds deregister_handler headers_changed [code $this headers_changed]
		$old_ds deregister_handler onchange [code $this onchange]
	}
	$itk_component(list) configure -datasource $ds

	if {$ds != {}} {
		$ds register_handler headers_changed [code $this headers_changed]
		$ds register_handler onchange [code $this onchange]
		set old_ds	$ds
		onchange
		headers_changed [$ds get_headers]
	} else {
		if {[info exists old_ds]} {
			unset old_ds
		}
	}
}


body tlc::DSpopup::constructor {args} { #<<<1
	itk_component add list {
		tlc::Browse_tktreectrl $w.list
	} {
		keep -filter_options -actionside -criteria -column_defaults
		keep -column_options -column_styles
		keep -textbackground
		keep -cursor -highlightbackground -highlightcolor
		keep -highlightthickness -foreground -backgroundimage -backgroundmode
		keep -buttonbitmap -buttoncolor -buttonimage -buttonsize
		keep -buttonthickness -columnprefix -columnproxy
		keep -columnresizemode -doublebuffer -height -indent
		keep -itemheight -itemprefix -itemwidth -itemwidthequal
		keep -itemwidthmultiple -linecolor -linestyle -linethickness
		keep -minitemheight -scrollmargin -selectmode -showbuttons -showheader
		keep -showlines -showroot -showrootbutton -showrootlines -treecolumn
		keep -usetheme -width -wrap -orient -xscrolldelay -xscrollincrement
		keep -yscrolldelay -yscrollincrement
		#keep -borderwidth -relief -takefocus
	}

	eval itk_initialize $args

	foreach reqf {parent} {
		if {![info exists $reqf]} {
			error "Must set -$reqf" "" [list missing_field $reqf]
		}
	}

	$w.list filter_add "Search" {spec entry}
	headers_changed [$ds get_headers]

	$w.list register_handler onselect_arr [code $this item_selected]
}


body tlc::DSpopup::destructor {} { #<<<1
	$ds deregister_handler headers_changed [code $this headers_changed]
	$ds deregister_handler onchange [code $this onchange]
}


body tlc::DSpopup::headers_changed {new_headers} { #<<<1
	set filter	{}
	if {$ds != {}} {
		foreach h $new_headers {
			lappend filter [string map \
					[list %field% [list $h]] \
					{[string match -nocase "*$filter(spec)*" $row(%field%)]}]
		}
	}
	$w.list filter_update "Search" [join $filter " ||\n"]
}


body tlc::DSpopup::item_selected {item_arr} { #<<<1
	$parent set_choice $item_arr
}


body tlc::DSpopup::onchange {} { #<<<1
	set row_cache	[$ds get_list {}]
}


