# vim: foldmarker=<<<,>>>

::itk::usual tlc::Myscrollbar {
	keep -background -cursor -troughcolor
	keep -activebackground -activerelief
	keep -highlightcolor -highlightthickness
	rename -highlightbackground -background background Background
}

option add *Myscrollbar.borderWidth			0		45
option add *Myscrollbar.relief				solid	45
option add *Myscrollbar.troughcolor			gray	45
option add *Myscrollbar.elementBorderWidth	1		45
option add *Myscrollbar.width				12		45

proc tlc::myscrollbar {pathName args} {
	uplevel tlc::Myscrollbar [list $pathName] $args
}

class tlc::Myscrollbar {
	inherit itk::Widget

	constructor {args} {}
	destructor {}

	itk_option define -mode mode ShowMode "fancy" repack
	itk_option define -orient orient Orient "vertical"

	public {
		method activate {args}		{uplevel $w.sb activate $args}
#		method cget {args}			{uplevel $w.sb cget $args}
#		method configure {args}		{uplevel $w.sb configure $args}
		method delta {args}			{uplevel $w.sb delta $args}
		method fraction {args}		{uplevel $w.sb fraction $args}
		method get {args}			{uplevel $w.sb get $args}
		method identify {args}		{uplevel $w.sb identify $args}
		method set {args}
	}
	
	private {
		variable w
		variable wdb
			
		variable shown	1

		method repack {}
	}
}


configbody tlc::Myscrollbar::orient { #<<<1
	$w.sb configure -orient $itk_option(-orient)
	repack
}


body tlc::Myscrollbar::constructor {args} { #<<<1
	::set w		$itk_interior
	::set wdb	[string range $w 1 end]

	itk_component add sb {
		ttk::scrollbar $w.sb
	} {
		keep -command
	}
	
	itk_component add blank {
		ttk::frame $w.blank
	} {
	}

	itk_initialize {*}$args

	repack
}


body tlc::Myscrollbar::set {args} { #<<<1
	if {[llength $args] == 2} {
		foreach {first last} $args {break}

		if {$first <= 0.0 && $last >= 1.0} {
			if {$shown} {
				::set shown	0
				repack
			}
		} else {
			if {!$shown} {
				::set shown	1
				repack
			}
		}
	}

	uplevel $w.sb set $args
}


body tlc::Myscrollbar::repack {} { #<<<1
	catch {blt::table forget $w.sb}
	catch {blt::table forget $w.blank}

	if {$shown || $itk_option(-mode) eq "static"} {
		blt::table $w $w.sb		1,1 -fill both
		switch [string index $itk_option(-orient) 0] {
			"v"	{
				blt::table configure $w c1 -width $w.sb
				blt::table configure $w r1 -height {}
			}

			"h"	{
				blt::table configure $w c1 -width {}
				blt::table configure $w r1 -height $w.sb
			}
		}
	} else {
		switch $itk_option(-mode) {
			fancy {
				blt::table $w $w.blank	1,1 -fill both
			}

			dynamic -
			vanishing {
			}
		}
		
		switch $itk_option(-mode) {
			static -
			dynamic -
			fancy {
				switch [string index $itk_option(-orient) 0] {
					"v"	{
						blt::table configure $w c1 -width $w.sb
						blt::table configure $w r1 -height {}
					}

					"h"	{
						blt::table configure $w c1 -width {}
						blt::table configure $w r1 -height $w.sb
					}
				}
			}

			vanishing {
				switch [string index $itk_option(-orient) 0] {
					"v"	{
						blt::table configure $w c1 -width {0 0 0}
						blt::table configure $w r1 -height {0 0 0}
					}

					"h"	{
						blt::table configure $w c1 -width {0 0 0}
						blt::table configure $w r1 -height {0 0 0}
					}
				}
			}
		} 
	}
}


