#!/usr/bin/itkwish3.1

# vim: ts=4 shiftwidth=4 ft=tcl foldmethod=marker foldmarker=<<<,>>>

wm withdraw .

package require Itk
namespace import -force ::itcl::*
namespace import -force ::itk::*
auto_mkindex_parser::slavehook { _%@namespace import -force ::itcl::* }
auto_mkindex_parser::slavehook { _%@namespace import -force ::itk::* }

package require TLC 0.9
namespace import tlc::*
eval	[go_home]

# Fudge the exit command <<<
rename exit _exit
proc exit {args} {
	puts stderr "Refusing to exit!"
}
#>>>

# Load template <<<
set fp	[open "template.n" r]
set template	[read $fp]
close $fp
#>>>

# Load documentation meat <<<
foreach file [glob -nocomplain -type f [file join meat *]] {
	set fp	[open $file r]
	set dat	[read $fp]
	close $fp
	regsub -all {;#.*$} $dat {} dat
	if {[catch {array set classinf $dat}]} {
		puts "Error parsing class info from file $file"
	} else {
		puts "Loaded classinf from $file"
	}
}
#>>>

# Helper funcs <<<
proc format_optlist {in} {
	set out		""
	set count	0
	foreach opt $in {
		append out	"\\$opt"
		
		if {[incr count] % 3 == 0} {
			append out	"\n"
		} else {
			append out	"\t"
		}

	}
	return $out
}


proc format_methodlist {in} {
	set out		""
	set count	0
	foreach method $in {
		append out	"\\fB${method}\\fR"
		
		if {[incr count] % 3 == 0} {
			append out	"\n"
		} else {
			append out	"\t"
		}

	}
	return $out
}


proc get_key {args} {
	set tmp		[array get ::classinf]
	foreach e $args {
		catch {unset walk}
		if {[catch {array set walk $tmp}]} {
			error "cannot parse ($tmp)"
		}
		if {![info exists walk($e)]} {
			error "path element ($e) not found in ($tmp)"
		}
		set tmp		$walk($e)
	}

	return $tmp
}
#>>>


set clean_list	{}
set not_clean	{}
set no_inf		{}
set errors		{}


foreach item $argv {
	if {[winfo exists .x]} {
		delete object .x
	}
	

	puts "\nGenerating template page for $item... "
	set clean		1

	if {[catch {$item .x} msg]} {
		puts "Cannot create $item.  Skipping ($msg)"
		lappend errors	$item
		continue
	}

	if {[catch {set myclass	[.x info class]}]} {
		set myclass	[winfo class .x]
	}

	if {![info exists classinf($myclass)]} {
		puts "ERROR: no info found for class $myclass!"
		lappend no_inf	$item
		continue
	}

	# Short description <<<
	if {[catch {set shortdesc [get_key $myclass shortdesc]}]} {
		set shortdesc	"Create and manipulate %NAME% widgets"
	}
	#>>>

	# Inheritance <<<
	set inheritance	".SH INHERITANCE\n"
	set heritage	{}
	if {[catch {set heritage	[.x info heritage]}]} {
		set inheritance	""
	} else {
		if {[llength $heritage] == 1} {
			append inheritance	"none"
		} else {
			append inheritance	$heritage
		}
	}
	#>>>

	# Options <<<
	set optlist				[.x configure]
	set alloptlist			""
	set optdesctext			""
	set count				0
	set inheritedoptstext	""
	catch {unset inheritedopts}
	array set inheritedopts	{}
	foreach opt $optlist {
		# Prune out synonyms
		switch [llength $opt] {
			2 {
				# Prune out synonyms
				continue
			}

			3 {
				# Convert non itk objects options
				set opt		[list [lindex $opt 0] {} {} [lindex $opt 1] [lindex $opt 2]]
			}
		}
	
		lappend alloptlist	[lindex $opt 0]

		# Prune out common options (documented in options.n)
		if {[lsearch {
			-activebackground -activeborderwidth -activeforeground
			-anchor -background -bitmap -borderwidth -cursor
			-disabledforeground -exportselection -font -foreground
			-highlightbackground -highlightcolor -highlightthickness
			-image -insertbackground -insertborderwidth -insertofftime
			-insertontime -insertwidth -jump -justify -orient -padx -pady
			-relief -repeatdelay -repeatinterval -selectbackground
			-selectborderwidth -selectforeground -setgrid -takefocus
			-text -textvariable -troughcolor -underline -wraplength
			-xscrollcommand -yscrollcommand -clientdata
		} [lindex $opt 0]] != -1} continue
		
		if {[lindex $opt 1] == {}} {set opt	[lreplace $opt 1 1 "N/A"]}
		if {[lindex $opt 2] == {}} {set opt	[lreplace $opt 2 2 "N/A"]}
		
		set found	""
		foreach class $heritage {
#			if {![info exists classinf($class)]} {
##				puts "WARNING: no class info found for ancestor: ${class}!"
#				continue
#			}
			if {![catch {set desc [get_key $class options [lindex $opt 0]]}]} {
				set desc	[string trim $desc]
				regsub -all {\n\s*} $desc "\n" desc
				set found	$class
				break
			}
		}

		if {$found == $myclass} {
			append optdesctext	".OP \\[lrange $opt 0 2]\n"
			append optdesctext	"$desc\n"
		} elseif {$found == ""} {
			append optdesctext	".OP \\[lrange $opt 0 2]\n"
			append optdesctext	"%FILLIN%\n"
			set clean	0
		} else {
			lappend inheritedopts($found)	[lindex $opt 0]
		}
	}
	if {$optdesctext != ""} {
		set optdesctext	".SH \"WIDGET-SPECIFIC OPTIONS\"\n$optdesctext"
	}
	set opttext		[format_optlist $alloptlist]
	if {[llength [array names inheritedopts]] != 0} {
		set inheritedoptstext	".SH \"INHERITED OPTIONS\"\n"
		foreach class $heritage {
			if {[info exists inheritedopts($class)]} {
				if {[catch {set manref [get_key $class command]}]} {
					set manref	$class
				}
				append inheritedoptstext ".LP\n.nf\n.ta 5.5c 11c\n.ft B\n"
				append inheritedoptstext [format_optlist $inheritedopts($class)]
				append inheritedoptstext "\n.fi\n.ft R\n.LP\n"
				append inheritedoptstext "See the \"$manref\" manual entry for "
				append inheritedoptstext "details on the above inherited "
				append inheritedoptstext "options.\n"
			}
		}
	}
	#>>>

	# Methods <<<
	catch {.x xxx} methodlist
	switch -glob $methodlist {
		"bad option \"xxx\": should be one of...\n*"
		{
			# Itk based widget
			set methodlist	[lrange [split $methodlist \n] 1 end]
		}

		"bad option \"xxx\": must be*"
		{
			# Tk type widget
			set len	[string length "bad option \"xxx\": must be "]
			set tmp	[string range $methodlist $len end]
			set tmp	[string map {, "" "or " ""} $tmp]
			set methodlist	{}
			puts "\n"
			foreach method $tmp {
				switch $method {
					"cget" -
					"configure" -
					"isa" -
					"component" -
					"config"	{continue}
				}

				if {
					(
					 [catch {.x $method} msg] ||
					 [catch {.x $method xxx} msg]
					) && [string match "wrong # args: should be *" $msg]
				} {
					puts "sniffed: ($msg)"
					regsub {^.*?\"(.*?)\"$} $msg {\1} msg
					puts "chopped: ($msg)"
					lappend methodlist $msg
				} else {
					puts "couldn't sniff"
					lappend methodlist ".x $method %FILLIN%"
				}
			}
		}

		default {
			puts stderr "Unknown methodlist type: ($methodlist)"
			set methodlist	{}
		}
	}

	set methoddesctext			""
	set inheritedmethodstext	""
	catch {unset inheritedmethods}
	array set inheritedmethods	{}
	foreach methodinf $methodlist {
#		puts "processing method: ($methodinf)"
		set method		[lindex $methodinf 1]
		set rest		[lrange $methodinf 2 end]
#			"component" -
		switch $method {
			"cget" -
			"configure" -
			"config" -
			"isa"		{continue}
		}

		set c	-1
		set idx	0
		set map	[list "?\\fI" "\\fR?"]
		while {[set idx	[string first "?" $rest $idx]] != -1} {
			set replacement [lindex $map [set c	[expr {($c+1)%2}]]]
			set rest	[string replace $rest $idx $idx $replacement]
			incr idx	4
		}
		
		set found	""
		foreach class $heritage {
			if {![catch {set desc [get_key $class methods $method]}]} {
				set desc	[string trim $desc]
				regsub -all {\n\s*} $desc "\n" desc
				set found	$class
				break
			}
		}
		
		if {$found == $myclass} {
			append methoddesctext	".TP\n"
			append methoddesctext	"\\fIpathName \\fB${method}\\fR $rest\n"
			append methoddesctext	"$desc\n"
		} elseif {$found == ""} {
			append methoddesctext	".TP\n"
			append methoddesctext	"\\fIpathName \\fB${method}\\fR $rest\n"
			append methoddesctext	"%FILLIN%\n"
			set clean	0
		} else {
			lappend inheritedmethods($found)	$method
		}
	}
	if {[llength [array names inheritedmethods]] != 0} {
		set inheritedmethodstext	".SH \"INHERITED METHODS\"\n"
		foreach class $heritage {
			if {[info exists inheritedmethods($class)]} {
				if {[catch {set manref [get_key $class command]}]} {
					set manref	$class
				}
				append inheritedmethodstext ".LP\n.nf\n.ta 5.5c 11c\n.ft B\n"
				append inheritedmethodstext [format_methodlist $inheritedmethods($class)]
				append inheritedmethodstext "\n.fi\n.ft R\n.LP\n"
				append inheritedmethodstext "see the \"$manref\" manual entry for "
				append inheritedmethodstext "details on the above inherited "
				append inheritedmethodstext "methods.\n"
			}
		}
		append inheritedmethodstext ".SH \"WIDGET-SPECIFIC METHODS\"\n"
	}
	#>>>

	# Components <<<
	set componentlist	{}
	catch {set componentlist	[.x component]}
	if {[llength $componentlist] != 0} {
		set components	".SH COMPONENTS\n"
	} else {
		set components	""
	}
	
	foreach component $componentlist {
		if {$component == "hull"} continue
		set class	[winfo class [.x component $component]]
		append components	".LP\n.nf\n"
		append components	"Name:\t\\fB${component}\\fR\n"
		append components	"Class:\t\\fB${class}\\fR\n"
		append components	".fi\n.IP\n"
		append components	"%FILLIN%\n"
	}
	if {$components == ".SH COMPONENTS\n"} {
		set components	""
	}
	#>>>

	# Example <<<
	if {![catch {set example [get_key $myclass example]}]} {
		set example		[string map [list "\\" "\\\\\\\\"] $example]
#		set example		[string map [list "&" "\\\\q&"] $example]
		set example		".SH EXAMPLE\n.ES\n${example}\n.EE"
	} else {
		set example		""
	}
	#>>>

	# Substitute data and write file <<<
	set data	$template
	
	regsub -all {%SHORTDESC%} $data $shortdesc data
	regsub -all {%INHERITANCE%} $data $inheritance data
	regsub -all {%OPTS%} $data $opttext data
	regsub -all {%INHERITEDOPTS%} $data $inheritedoptstext data
	regsub -all {%OPTDESC%} $data $optdesctext data
	regsub -all {%INHERITEDMETHODS%} $data $inheritedmethodstext data
	regsub -all {%METHODDESC%} $data $methoddesctext data
	regsub -all {%COMPONENTS%} $data $components data
	regsub -all {%EXAMPLE%} $data $example data
	regsub -all {%NAME%} $data $item data

	set fn	[file join generated "[string tolower $item].n"]
	set fp	[open $fn w]
	puts $fp $data
	close $fp
	puts "wrote $fn [lindex {{NOT CLEAN} clean} $clean]"
	#>>>

	if {$clean} {
		lappend clean_list	$item
	} else {
		lappend not_clean	$item
	}
}


puts "\nResults:\n"
puts "clean: ([llength $clean_list]):\n[join $clean_list \n]\n"
puts "not_clean: ([llength $not_clean]):\n[join $not_clean \n]\n"
puts "errors: ([llength $errors]):\n[join $errors \n]\n"
puts "no_inf: ([llength $no_inf]):\n[join $no_inf \n]\n"


_exit 0


